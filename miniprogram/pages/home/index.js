const app = getApp();
const Utils = require('../../utils/index');
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
// 在页面中定义插屏广告
let interstitialAd = null;
// 在页面中定义激励视频广告
let videoAd = null;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    link_url: '',
    cover: '',
    bg_music: '',
    video_url: '',
    show_video: 0,
    downloading: false,
    download_progress: 0,
    show_ad_popup: 0,
    tempFilePath: '',
    show_action_sheet: false,
    action_list: [
      { name: '推荐给好友', openType: 'share', subname: '分享小程序给好友', color: 'red' },
      { name: '转发视频', type: 'SHARE_VIDEO', subname: '转发视频给好友', color: 'orange' },
      { name: '领取红包', type: 'RED_PACKET', subname: '点击领取红包', color: 'red' },
    ],
  },

  /**
   * 初始化激励视频广告
   * @param {*} adUnitId 广告位 id
   */
  initRewardedVideoAd: function (adUnitId) {
    if (!wx.createRewardedVideoAd) return null;
    const that = this;
    const videoAd = wx.createRewardedVideoAd({ adUnitId });
    videoAd.onLoad(() => {})
    videoAd.onError((err) => {})
    videoAd.onClose((res) => {
      const { isEnded } = res;
      if (isEnded) {
        Utils.updateViewAdTimesFunc();
        console.log('开始下载。。。。。');
        that.startDownloadAction();
      } else {
        wx.showModal({
          title: '失败',
          content: '没有看完广告，看完视频广告才能获取奖励，免费使用',
          showCancel: false,
        });
      }
    });
    return videoAd;
  },

  /**
   * 初始化插屏广告
   * @param {*} adUnitId adUnitId 广告位 id
   */
  initInterstitialAd: function (adUnitId) {
    if (!wx.createInterstitialAd) return null;
    const interstitialAd = wx.createInterstitialAd({ adUnitId });
    interstitialAd.onLoad(() => {});
    interstitialAd.onError((err) => {});
    interstitialAd.onClose(() => { });
    return interstitialAd;
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    videoAd = this.initRewardedVideoAd('adunit-c923da289178ef14');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 在页面onLoad回调事件中创建插屏广告实例
    // interstitialAd = this.initInterstitialAd('adunit-33e9d3c21b1bef10');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 保存视频
   */
  saveVideoAction: function () {
    // 用户触发广告后，显示激励视频广告 - 弹框告知用户广告显示规则
    // if (show_videoAd && videoAd) return this.setData({ show_ad_popup: 1 });
    wx.showLoading({ title: '初始化资源' });
    const that = this;
    Utils.checkIfFreeAdFunc().then(freeAd => {
      if (freeAd === 0) {
        wx.showModal({
          title: '温馨提示',
          content: '观看1次视频广告，24小时内下载免广告',
          cancelText: '下次再说',
          confirmText: '观看广告',
          success(e) {
            if (e.confirm) {
              videoAd.show().catch(() => {
                videoAd.load().then(() => { return videoAd.show(); }).catch(() => { that.startDownloadAction() });
              });
            } else {
              return console.log("用户点击取消");
            }
          },
          complete() {
            wx.hideLoading();
          }
        });
      } else {
        this.startDownloadAction();
      }
      wx.hideLoading();
    }).catch(err => {
      console.error('downloadFileAction', err);
      wx.hideLoading();
    });
  },

  /**
   * 开始下载
   */
  startDownloadAction: function () {
    console.log('startDownloadAction 开始下载');
    Utils.authorize('scope.writePhotosAlbum')
      .then(result => {
        console.log('1: result', result);
        const { errMsg } = result;
        if (errMsg !== 'authorize:ok') throw Error(errMsg);
        // 获取下载链接
        this.setData({ downloading: true, download_progress: 0 });
        const { video_url } = this.data;
        // 1. 判断域名是否已经包含在安全域名列表中
        // 2. 不在列表中，使用 cloudDownloadFile
        // 3. 在列表中，使用 downloadVideoFile
        const hostname = Utils.getHostnameFromLink(video_url);
        if (Utils.checkHasDomain(hostname)) {
          const d_url = video_url.replace('http://', 'https://');
          return { result: { download_url: d_url, local: true } };
        } else {
          return wx.cloud.callFunction({ name: 'download_action', data: { file_url: video_url } });
        }
      })
      .then(result => {
        console.log('2: result', result);
        const { download_url, local } = result.result;
        // 下载视频
        if (local) {
          console.log('前端下载', local);
          return Utils.downloadVideoFile(download_url, obj => {
            this.setData({ download_progress: obj.progress });
          });
        } else {
          console.log('云端下载', local);
          return Utils.cloudDownloadFile(download_url, obj => {
            this.setData({ download_progress: obj.progress });
          });
        }
      })
      .then(result => {
        console.log('3: 下载', result);
        this.setData({ downloading: false, tempFilePath: result.tempFilePath });
        return Utils.saveVideoToPhotosAlbum(result.tempFilePath);
      })
      .then(() => {
        const content = `成功保存到手机相册。永不收费，不限次数，微信搜索：${app.app_name}`;
        wx.showModal({ title: '保存成功', content, confirmText: '好的', showCancel: false });
      })
      .catch(error => {
        console.log('catch error', error);
        const { errMsg } = error;
        if ((errMsg||'').startsWith('authorize:fail')) {
          Dialog.confirm({
            title: '提示',
            message: '保存失败，请授权「相册」后重新保存',
            showCancelButton: true,
            confirmButtonText: '去授权',
            confirmButtonOpenType: 'openSetting'
          });
        } else {
          wx.showModal({ title: '提示', content: '下载失败，请重试', confirmText: '重试', showCancel: false }).then(res => {
            if (res.confirm) this.startDownloadAction();
          }).catch(err => {
            console.log('取消');
          });
        };
        this.setData({ downloading: false });
        console.error(error.errMsg);
      });
  },

  /**
   * 粘贴视频链接
   */
  pasteLinkAction: function() {
    const that = this;
    wx.getClipboardData({
      success: (option) => {
        const index = option.data.indexOf('https://');
        if (option.data.length <= 0 || index < 0) {
          // Dialog.alert({ title: '提示', message: '粘贴板没有链接，请先复制链接' });
          wx.showModal({
            title: '提示',
            content: '粘贴板没有链接，请先复制链接',
            showCancel: false
          });
          return;
        }
        that.setData({ link_url: option.data });
      },
    });
  },

  /**
   * 复制链接
   * video - 视频链接
   * bgm - 背景音乐链接
   * cover - 封面链接
   */
  copyLinkAction: function (res) {
    const { linkType } = res.currentTarget.dataset;
    const map = { video: 'video_url', bgm: 'bg_music', cover: 'cover' };
    const link = this.data[map[linkType]];
    if (!link) return;
    wx.setClipboardData({
      data: link,
    });
  },

  /**
   * 清空链接
   */
  cleanLinkAction: function () {
    this.setData({ link_url: '', show_video: 0 });
  },

  /**
   * 复制客服微信号
   */
  copyContactWechatAction: function () {
    wx.setClipboardData({
      data: 'dafish1212',
    });
  },

  /**
   * 去除水印
   * 1. 未授权需要提示授权
   */
  removeWaterMark: function () {
    const { link_url } = this.data;
    if (link_url.length <= 0) Toast('需要粘贴地址！');
    const index = link_url.indexOf('https://');
    if (index < 0) return Toast('地址不太对哦');
    Toast.loading({ mask: true, message: '处理中...', duration: 8000 });
    wx.cloud.callFunction({ name: 'remove_watermark_v3', data: { link_url } }).then(res => {
      console.log(res.result);
      const { content_type, code, cover, url, music, msg, title } = res.result;
      Toast.clear();
      if (code !== 0) {
        Dialog.alert({ title: '解析失败', message: msg || '解析失败，请重试' });
        return;
      }
      // 本地缓存解析结果信息
      const record = { title, content_type, link_url };
      this.saveParseRecord(record);
      // 打开去除水印后的视频/图集页面
      app.globalData['K_VIDEO_INFO'] = res.result;
      if (content_type === 'PICS') {
        // 图集，提示用户需要跳转到图集详情页面
        const PAGE_MAP = { VIDEO: '/pages/detail/index', PICS: '/pages/pic_detail/index' };
        wx.navigateTo({ url: PAGE_MAP[content_type] });
        return;
      }
      this.setData({ video_url: url, cover, show_video: 1, bg_music: music });
    }).catch(err => {
      console.log(err);
      Toast.clear();
      Dialog.alert({ title: '提示', message: '解析失败，请重试' });
    });
  },

  /**
   * 本地缓存解析结果
   * @param {*} data data
   */
  saveParseRecord: function (data) {
    const Key = 'PARSE_RECORD';
    const Max_Len = app.globalData.parse_record_max_len || 30;
    data.created_time = new Date();
    Utils.getStorage(Key, [])
    .then(res => {
      const record_list = res.data || [];
      if (record_list.length >= Max_Len) record_list.shift();
      record_list.push(data);
      return Utils.setStorage(Key, record_list);
    })
    .catch(err => {
      console.error('saveParseRecord', err);
    });
  },

  /**
   * 输入内容
   */
  areaInputFunc: function(res) {
    this.setData({ link_url: res.detail });
  },

  /**
   * 打开使用教程
   */
  showHelpAction: function () {
    wx.navigateTo({
      url: '/pages/help/index',
    });
  },

  /**
   * 打开使用教程
   */
  showQuestionAction: function () {
    wx.navigateTo({
      url: '/pages/question/index',
    });
  },

  /**
   * 打开历史记录
   */
  showHistoryAction: function () {
    const that = this;
    wx.navigateTo({
      url: '/pages/history/index',
      events: {
        acceptRecordData (data) {
          console.log('选择了记录信息', data);
        },
      },
    });
  },

  /**
   * 关闭激励视频广告观看提示弹框
   */
  onAdPopupCloseAction: function () {
    this.setData({ show_ad_popup: 0 });
  },

  /**
   * 展示视频广告
   */
  showVideoAdAction: function () {
    this.setData({ show_ad_popup: 0 });
    if (videoAd) {
      videoAd.show().catch(() => {
        videoAd.load().then(() => videoAd.show()).catch(err => { console.log('激励视频 广告显示失败') });
      });
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 关闭 action sheet
   */
  onActionSheetClose() {
    this.setData({ show_action_sheet: false });
  },

  /**
   * 点击 action sheet 项
   * @param {*} event e
   */
  onActionSheetSelect(event) {
    const { type } = event.detail;
    if (type === 'SHARE_VIDEO' && this.data.tempFilePath) {
      this.shareVideoMessage();
    } else if (type === 'RED_PACKET') {
      wx.navigateToMiniProgram({
        appId: 'wxde8ac0a21135c07d',
        path: 'waimaiunion/pages/union/index?scene=1!f4hVVr5zJG_R!1!2!sVdWvg',
      })
    }
  },

  /**
   * 转发视频到聊天
   */
  shareVideoMessage() {
    wx.shareVideoMessage({ videoPath: this.data.tempFilePath }).catch(err => console.error(err));
  },

  /**
   * 组件加载成功时触发
   * @param {*} res 
   */
  onOfficialAccountLoad: function (res) {
    console.log('official-account', res);
  },

  /**
   * 组件加载失败时触发
   * @param {*} err 
   */
  onOfficialAccountError: function (err) {
    console.error('official-account', err);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    const share_scene = 'home';
    const share_title = '免费、免广告的短视频去水印工具';
    const home_page = '/pages/home/index';
    const share_info = {
        title: share_title,
        path: `${home_page}?share_scene=${share_scene}`,
        // imageUrl: 'cloud://summer-0gan6wz86e232ed4.7375-summer-0gan6wz86e232ed4-1306003894/pp/share-logo.png',
        success: function(e) {},
        fail: function(e) {},
        complete: function() {}
    };
    return share_info;
  }
})